package com.example.statemanagementdemo

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.statemanagementdemo.databinding.LayoutFibonacciItemBinding

class FibonacciAdapter(
    private val listener: OnFibonacciItemClickListener? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var activeItemIndex = -1
    private val items: MutableList<FibonacciItem> = mutableListOf()

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        LayoutInflater
            .from(parent.context)
            .let { LayoutFibonacciItemBinding.inflate(it, parent, false) }
            .let { FibonacciViewHolder(it, listener) }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        items.getOrNull(position)
            ?.also { data ->
                if (holder is FibonacciViewHolder) {
                    holder.setData(data, activeItemIndex)
                }
            }
    }

    @Suppress("NotifyDataSetChanged")
    fun updateItems(data: List<FibonacciItem>, activeItemIndex: Int) {
        this.activeItemIndex = activeItemIndex
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    interface OnFibonacciItemClickListener {
        fun onFibonacciItemClicked(item: FibonacciItem)
    }

    class FibonacciViewHolder(
        private val binding: LayoutFibonacciItemBinding,
        private val listener: OnFibonacciItemClickListener? = null
    ) : RecyclerView.ViewHolder(binding.root) {

        fun setData(data: FibonacciItem, activeItemIndex: Int) {
            val res = itemView.resources
            val strId = R.string.fibonacci_item_label
            val label = res.getString(strId, data.index, data.number)
            binding.txtLabel.text = label

            val drawableImage = when (data.symbol) {
                SymbolType.SQUARE -> ResourcesCompat.getDrawable(res, R.drawable.ic_square_20, null)
                SymbolType.CIRCLE -> ResourcesCompat.getDrawable(res, R.drawable.ic_circle_20, null)
                SymbolType.CROSS -> ResourcesCompat.getDrawable(res, R.drawable.ic_cross_20, null)
            }
            binding.imgSymbol.setImageDrawable(drawableImage)

            val bgColor = if (data.index == activeItemIndex) Color.RED else Color.WHITE
            itemView.rootView.setBackgroundColor(bgColor)

            itemView.setOnClickListener {
                listener?.onFibonacciItemClicked(data)
            }
        }
    }
}
