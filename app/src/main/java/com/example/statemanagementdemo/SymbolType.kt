package com.example.statemanagementdemo

enum class SymbolType {
    CIRCLE, CROSS, SQUARE
}