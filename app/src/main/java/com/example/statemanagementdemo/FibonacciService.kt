package com.example.statemanagementdemo

object FibonacciService {

    fun getFibonacciNumbers(limit: Int = 100): List<FibonacciItem> {
        val data = mutableListOf<FibonacciItem>()
        for (index in 0 until limit) {
            val expectedNumber = when (index) {
                0 -> 0
                1 -> 1
                else -> data[index - 2].number + data[index - 1].number
            }

            val expectedSymbol = SymbolType.entries.shuffled().first()
            val item = FibonacciItem(
                index = index,
                number = expectedNumber,
                symbol = expectedSymbol
            )

            data.add(index, item)
        }

        return data
    }
}