package com.example.statemanagementdemo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel : ViewModel() {

    private val _fibonacciNumbers = MutableLiveData<List<FibonacciItem>?>()
    private val _selectedItem = MutableLiveData<List<FibonacciItem>?>()
    val selectedItem: LiveData<List<FibonacciItem>?>
        get() = _selectedItem

    val mainUiLiveData = MediatorLiveData<List<FibonacciItem>?>()
    var recentlyRestoreItem: FibonacciItem? = null

    init {
        mainUiLiveData.apply {
            fun setData() {
                val selectedItems = _selectedItem.value ?: listOf()
                val filtered = _fibonacciNumbers.value
                    ?.filter { it !in selectedItems }
                    ?.sortedBy { it.index }

                value = filtered ?: listOf()
            }

            addSource(_fibonacciNumbers) { setData() }
            addSource(_selectedItem) { setData() }
            setData()
        }
    }

    fun getFibonacciNumbers() {
        viewModelScope.launch(Dispatchers.IO) {
            var data: List<FibonacciItem>? = null
            try {
                data = FibonacciService.getFibonacciNumbers(limit = 45)

            } catch (e: Throwable) {
                e.printStackTrace()

            } finally {
                withContext(Dispatchers.Main) {
                    recentlyRestoreItem = null
                    _fibonacciNumbers.value = data
                }
            }
        }
    }

    fun appendSelectedItem(item: FibonacciItem) {
        _selectedItem.apply {
            if (value?.contains(item) != true) {
                recentlyRestoreItem = null
                val newList = value?.toMutableList()?.apply { add(item) }?.sortedBy { it.index }
                postValue(newList ?: listOf(item))
            }
        }
    }

    fun removeSelectedItem(item: FibonacciItem) {
        _selectedItem.apply {
            if (value?.contains(item) == true) {
                recentlyRestoreItem = item
                val newList = value?.toMutableList()?.apply { remove(item) }
                postValue(newList)
            }
        }
    }
}
