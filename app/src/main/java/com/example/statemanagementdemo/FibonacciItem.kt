package com.example.statemanagementdemo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FibonacciItem(
    val index: Int,
    val number: Int,
    val symbol: SymbolType
) : Parcelable
