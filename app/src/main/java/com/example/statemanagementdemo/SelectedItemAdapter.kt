package com.example.statemanagementdemo

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.statemanagementdemo.databinding.LayoutSelectedItemBinding

class SelectedItemAdapter(
    private val listener: OnSelectedItemClickListener? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var activeItemIndex = -1
    private val items: MutableList<FibonacciItem> = mutableListOf()

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        LayoutInflater
            .from(parent.context)
            .let { LayoutSelectedItemBinding.inflate(it, parent, false) }
            .let { SelectedItemViewHolder(it, listener) }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        items.getOrNull(position)
            ?.also { data ->
                if (holder is SelectedItemViewHolder) {
                    holder.setData(data, activeItemIndex)
                }
            }
    }

    @Suppress("NotifyDataSetChanged")
    fun updateItems(data: List<FibonacciItem>, activeItemIndex: Int) {
        this.activeItemIndex = activeItemIndex
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    interface OnSelectedItemClickListener {
        fun onSelectedItemClicked(data: FibonacciItem)
    }

    class SelectedItemViewHolder(
        private val binding: LayoutSelectedItemBinding,
        private val listener: OnSelectedItemClickListener? = null
    ) : RecyclerView.ViewHolder(binding.root) {

        fun setData(data: FibonacciItem, activeItemIndex: Int) {
            val txtNumber = "Number: ${data.number}"
            binding.txtNumber.text = txtNumber

            val txtIndex = "Index: ${data.index}"
            binding.txtIndex.text = txtIndex

            val res = itemView.resources
            val drawableImage = when (data.symbol) {
                SymbolType.SQUARE -> ResourcesCompat.getDrawable(res, R.drawable.ic_square_20, null)
                SymbolType.CIRCLE -> ResourcesCompat.getDrawable(res, R.drawable.ic_circle_20, null)
                SymbolType.CROSS -> ResourcesCompat.getDrawable(res, R.drawable.ic_cross_20, null)
            }
            binding.imgSymbol.setImageDrawable(drawableImage)

            val bgColor = if (data.index == activeItemIndex) Color.GREEN else Color.WHITE
            itemView.rootView.setBackgroundColor(bgColor)

            itemView.setOnClickListener {
                listener?.onSelectedItemClicked(data)
            }
        }
    }
}
