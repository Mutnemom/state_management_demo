package com.example.statemanagementdemo

import android.app.Dialog
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.statemanagementdemo.databinding.LayoutSelectedItemBottomSheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class SelectedItemBottomSheet :
    BottomSheetDialogFragment(),
    SelectedItemAdapter.OnSelectedItemClickListener {

    companion object {
        private const val KEY_SELECTED_ITEMS = "selected-items"

        @JvmStatic
        fun newInstance(newEntry: FibonacciItem) = SelectedItemBottomSheet()
            .apply {
                arguments = Bundle().apply {
                    putParcelable(KEY_SELECTED_ITEMS, newEntry)
                }
            }
    }

    private lateinit var binding: LayoutSelectedItemBottomSheetBinding
    private lateinit var mainVm: MainViewModel
    private lateinit var selectedItemAdapter: SelectedItemAdapter
    private var newEntry: FibonacciItem? = null

    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        newEntry = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            arguments?.getParcelable(KEY_SELECTED_ITEMS, FibonacciItem::class.java)
        } else {
            arguments?.getParcelable(KEY_SELECTED_ITEMS)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LayoutSelectedItemBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupRecyclerView()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val sheet = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        sheet.setOnShowListener { d ->
            (d as? BottomSheetDialog)?.apply {
                val view =
                    findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)

                val maxHeight = (Resources.getSystem().displayMetrics.heightPixels * 0.4).toInt()
                view?.apply {
                    layoutParams = layoutParams.apply { height = maxHeight }
                }
            }
        }
        return sheet
    }

    override fun onSelectedItemClicked(data: FibonacciItem) {
        mainVm.removeSelectedItem(data)
        this.dismiss()
    }

    private fun setupViewModel() {
        mainVm = ViewModelProvider(
            requireActivity(),
            MainViewModelFactory()
        )[MainViewModel::class.java].apply {
            selectedItem.observe(viewLifecycleOwner) { dataSet ->
                val selectedSymbol = newEntry?.symbol ?: return@observe
                val selectedItemIndex = newEntry?.index ?: -1
                dataSet?.filter { it.symbol == selectedSymbol }
                    ?.sortedBy { it.index }
                    ?.also { selectedItemAdapter.updateItems(it, selectedItemIndex) }
                    ?.indexOf(newEntry)
                    ?.also { requestScrollToPosition(it) }
            }
        }
    }

    private fun requestScrollToPosition(position: Int) {
        if (position in 0 until selectedItemAdapter.itemCount) {
            binding.recyclerBottomSheet.post {
                binding.recyclerBottomSheet.scrollToPosition(position)
            }
        }
    }

    private fun setupRecyclerView() {
        selectedItemAdapter = SelectedItemAdapter(this)
        binding.recyclerBottomSheet.apply {
            adapter = selectedItemAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }
}