package com.example.statemanagementdemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.statemanagementdemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), FibonacciAdapter.OnFibonacciItemClickListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mainVm: MainViewModel
    private lateinit var fibonacciAdapter: FibonacciAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        setupRecyclerView()
    }

    override fun onFibonacciItemClicked(item: FibonacciItem) {
        mainVm.appendSelectedItem(item)
        displayBottomSheet(item)
    }

    private fun setupViewModel() {
        mainVm = ViewModelProvider(this, MainViewModelFactory())[MainViewModel::class.java]
            .apply {
                mainUiLiveData.observe(this@MainActivity) { dataSet ->
                    dataSet?.also {
                        val activeItemIndex = recentlyRestoreItem?.index ?: -1
                        fibonacciAdapter.updateItems(it, activeItemIndex)
                        requestScrollToPosition(it.indexOf(recentlyRestoreItem))
                    }
                }
            }

        mainVm.getFibonacciNumbers()
    }

    private fun setupRecyclerView() {
        fibonacciAdapter = FibonacciAdapter(this)
        binding.recyclerMain.apply {
            adapter = fibonacciAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    private fun displayBottomSheet(newEntry: FibonacciItem) {
        SelectedItemBottomSheet.newInstance(newEntry).show(supportFragmentManager, "")
    }

    private fun requestScrollToPosition(position: Int) {
        if (position in 0 until fibonacciAdapter.itemCount) {
            binding.recyclerMain.post {
                binding.recyclerMain.scrollToPosition(position)
            }
        }
    }
}
